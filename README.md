# python-sardana-kitslabpandabox

PandABox Trigger Gate Controller for testings purpose at kitslab.

# PandABox documentation
- All blocks available in the PandABox pallete can be checked out on this documentation page: https://pandablocks-fpga.readthedocs.io/en/latest/blocks.html
- The python libraries used to communicate with PandA through control and data ports can be checked out here: https://github.com/PandABlocks

## Demonstration / pair-programming
Develop together a Sardana TriggerGateController (luckily many people from Sardana community around!) supporting different kind of scans. The idea is to do one scan type per time. After, add a fast shutter control. If possible, use a motor controller for position based scans. Finally, merge everything together in the same controller.

- **use PCAP for data acquisition**: it can have as many experimental channels as required.

# Synchronization on time domain
## Trigger/Gate controller
The trigger/gate controller class `TimeBasedTriggerGateCtrl` was developed together during the demo. It supports synchronization time-based, including fast shutter control. Figure 1 illustrates the PandABox design implemented to generate triggers to experimental channels, including the fast shutter control.

![Time-based PandABox Layout](/docs/timebased.png)
*Figure 1: Time-based + shutter control PandABox layout*

## JSON configuration file
The JSON configuration file related to the PandABox design illustrated on Figure 1 can be checked out in file **/docs/timebasedpandaboxdesign.json**. For example, the PULSE1 block used to the generate triggers to the experimental channels is described in the file as follows:

```json
"PULSE1": {
      "label": "pulse train to detectors",
      "inputs": "expanded",
      "enable": "BITS1.OUTA",
      "enableDelay": 0,
      "trig": "TTLIN1.VAL",
      "trigDelay": 0,
      "parameters": "expanded",
      "delay": 0.0,
      "delayUnits": "s",
      "width": 1.0,
      "widthUnits": "s",
      "pulses": 11,
      "step": 2.0,
      "stepUnits": "s",
      "trigEdge": "Falling",
      "outputs": "expanded",
      "readbacks": "expanded"
    },
```

# Synchronization on position domain
The trigger/gate controller class `PositionBasedTriggerGateCtrl` supports synchronization position-based. The Figure 2 illustrates the PandABox design implemented to generate triggers to experimental channels based on the motor position. The motor encoder position is read with the block INENC1, while the block SEQ2 contains the positions configuration in which a trigger should be generated.

![Position-based PandABox Layout](/docs/positionbased.png)
*Figure 2: Position-based PandABox layout*


