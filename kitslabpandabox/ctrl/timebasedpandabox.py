#!/usr/bin/env python

from sardana.pool.pooldefs import SynchDomain, SynchParam
from sardana.pool.controller import (
    TriggerGateController,
    Type,
    Access,
    Description,
    DefaultValue,
    DataAccess,
    FGet,
    FSet,
    Memorize,
    Memorized,
)
from pandablocks.blocking import BlockingClient
from pandablocks.commands import Get, Put
from tango import DevState


class TimeBasedTriggerGateCtrl(TriggerGateController):
    """
    TriggerGateController for kitslab PandABox control for synchronization on
    time domain including shutter control
    """

    organization = "MAXIV"
    gender = "TriggerGate"
    model = "PandABox"

    ctrl_properties = {
        "hostname": {Type: str, Description: "PandABox hostname"},
    }

    ctrl_attributes = {
        "useShutterFeedback": {
            Type: bool,
            Description: "True: use shutter feedback. False: use fixed delay time after shutter trigger.",
            Access: DataAccess.ReadWrite,
            Memorize: Memorized,
            FGet: "get_useShutterFeedback",
            FSet: "set_useShutterFeedback",
            DefaultValue: True,
        },
        "shutterDelay": {
            Type: float,
            Description: "Delay time between shutter trigger and the start of the data acquisition.",
            Access: DataAccess.ReadWrite,
            FGet: "get_shutterDelay",
            FSet: "set_shutterDelay",
            DefaultValue: 0,
        },
    }

    def __init__(self, inst, props, *args, **kwargs):
        """connect to PandABox control port, access to Functional Blocks config"""
        TriggerGateController.__init__(self, inst, props, *args, **kwargs)
        self._state = None
        self._status = None
        self.panda = BlockingClient(self.hostname)
        self.panda.connect()
        self.use_feedback = True
        self.shutter_delay = 0

    def AddDevice(self, axis):
        pass

    def DeleteDevice(self, axis):
        """
        immediately stop any triggering and close connection to PandABox
        """
        self.enable_blocks(0)
        self.panda.close()

    def PrepareOne(self, axis, nb_starts):
        """
        this method can be used to enable/disable and prepare blocks only once
        for the all scan
        """
        self.nb_starts = nb_starts

    def SynchOne(self, axis, configuration):
        """
        prepare and configure all PandABox blocks to be used for the scan
        """
        self.enable_blocks(0)  # stop any triggering
        group = configuration[0]
        num_points = group[SynchParam.Repeats]
        exposure_time = group[SynchParam.Active][SynchDomain.Time]
        total_time = group[SynchParam.Total][SynchDomain.Time]

        # --- support for scan on time domain --- #
        # configure pulse block for experimental channels trigger
        self.panda.send(Put("PULSE1.PULSES", f"{num_points}"))
        self.panda.send(Put("PULSE1.STEP", f"{total_time}"))
        self.panda.send(Put("PULSE1.WIDTH", f"{exposure_time}"))

        # shutter open/close between each scan start, e.g.: if ascan, it will
        # open/close between each motor step. if timescan, it will open in the beginning
        # of the scan and close in the end after the repetitions are performed.
        self.panda.send(Put("COUNTER1.MAX", f"{num_points-1}"))

        if self.use_feedback:
            # data acquisition start after shutter feedback signal is received in PandA
            self.panda.send(Put("PULSE1.DELAY", "0"))
        else:
            # data acquisition start after a delay time, don't rely on the feedback signal
            self.panda.send(Put("PULSE1.DELAY", f"{self.shutter_delay}"))

        # enable blocks only. it doesn't start any triggering
        self.enable_blocks(1)

    def PreStartOne(self, axis):
        """PreStart the specified trigger"""
        return True

    def StartOne(self, axis):
        """
        start PandABox master trigger
        """
        # send a rising flank from bits block
        self.panda.send(Put("BITS1.B", "0"))
        self.panda.send(Put("BITS1.B", "1"))

    def StateAll(self):
        """
        Get the trigger/gate state
        """
        try:
            if bool(int(self.panda.send(Get("PULSE1.QUEUED")))):
                self._state = DevState.RUNNING
                self._status = "PandABox is in RUNNING state: triggering ..."
            else:
                self._state = DevState.STANDBY
                self._status = (
                    "PandABox is in STANDBY state: waiting for trigger command. "
                )
        except Exception as e:
            self._state = DevState.FAULT
            self._status = f"PandABox is in FAULT state: {e}"

    def StateOne(self, axis=1):
        """
        Get the trigger/gate state
        """
        return self._state, self._status

    def AbortOne(self, axis):
        """Stop any triggering"""
        self.enable_blocks(0)

    #### ---- Controller Attributes ---- ####

    def get_useShutterFeedback(self):
        return self.use_feedback

    def set_useShutterFeedback(self, value):
        """
        set if data acquisition should be triggered by shutter feedback signal
        OR should be triggered after defined delay time
        """
        if value:
            self.use_feedback = True
            self.panda.send(Put("BITS1.C", "0"))
        else:
            self.use_feedback = False
            self.panda.send(Put("BITS1.C", "0"))
            self.panda.send(Put("BITS1.C", "1"))

    def get_shutterDelay(self):
        return self.shutter_delay

    def set_shutterDelay(self, value):
        """
        set the delay time between shutter output trigger and the start of the
        data acquisition, if the shutter feedback signal is not the master trigger
        """
        self.shutter_delay = value

    #### ---- Helpers ---- ####

    def enable_blocks(self, value):
        """
        enable/disable blocks according to the scan type
        """
        if value:
            self.panda.send(Put("BITS1.A", "0"))
            self.panda.send(Put("BITS1.A", "1"))
        else:
            self.panda.send(Put("BITS1.A", "0"))
