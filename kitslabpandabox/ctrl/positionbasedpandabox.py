#!/usr/bin/env python

from sardana.pool.pooldefs import SynchDomain, SynchParam
from sardana.pool.controller import (
    TriggerGateController,
    Type,
    Description,
)
from pandablocks.blocking import BlockingClient
from pandablocks.commands import Put
from tango import DevState, DeviceProxy
import numpy as np


class PositionBasedTriggerGateCtrl(TriggerGateController):
    """
    TriggerGateController for kitslab PandABox control for synchronization on position domain
    """

    organization = "MAXIV"
    gender = "TriggerGate"
    model = "PandABox"

    ctrl_properties = {
        "hostname": {Type: str, Description: "PandABox hostname"},
        "motor": {
            Type: str,
            Description: "motor name to be used in continuous scan position based",
        },
    }

    ctrl_attributes = {}

    def __init__(self, inst, props, *args, **kwargs):
        """connect to PandABox control port, access to Functional Blocks config"""
        TriggerGateController.__init__(self, inst, props, *args, **kwargs)
        self._state = None
        self._status = None
        self.panda = BlockingClient(self.hostname)
        self.panda.connect()
        self.motor_device = DeviceProxy(self.motor)

    def AddDevice(self, axis):
        pass

    def DeleteDevice(self, axis):
        """
        immediately stop any triggering and close connection to PandABox
        """
        self.enable_blocks(0)
        self.panda.close()

    def PrepareOne(self, axis, nb_starts):
        """
        this method can be used to enable/disable and prepare blocks only once
        for the all scan
        """
        self.nb_starts = nb_starts

    def SynchOne(self, axis, configuration):
        """
        prepare and configure all PandABox blocks to be used for the scan
        """
        self.enable_blocks(0)  # stop any triggering
        group = configuration[0]
        num_points = group[SynchParam.Repeats]
        exposure_time = group[SynchParam.Active][SynchDomain.Time]
        start_pos_mm = group[SynchParam.Initial][SynchDomain.Position]
        total = group[SynchParam.Total][SynchDomain.Position]  # step size
        final_pos_mm = start_pos_mm + (total * (num_points - 1))

        start_pos_counts = self._mm_to_encoder_counts(start_pos_mm)
        final_pos_counts = self._mm_to_encoder_counts(final_pos_mm)

        # array of positions in encoder counts
        self.pos_array_counts = np.linspace(
            start_pos_counts, final_pos_counts, num=num_points
        )
        self.positions = np.rint(self.pos_array_counts)

        # --- support for scans on position domain --- #

        # define direction (pos in Ev)
        if start_pos_mm < final_pos_mm:
            trigger = "posa<=position"
        else:
            trigger = "posa>=position"

        # user options for SEQ table
        repeats = 1
        time1 = 1
        phase1 = {
            "a": True,
            "b": False,
            "c": False,
            "d": False,
            "e": False,
            "f": False,
        }
        time2 = 1
        phase2 = {
            "a": False,
            "b": False,
            "c": False,
            "d": False,
            "e": False,
            "f": False,
        }

        self.pandabox.send(
            Put(
                "SEQ2.TABLE",
                self.seq_table(
                    repeats, trigger, self.positions, time1, phase1, time2, phase2
                ),
            )
        )

        # set integration time in seconds to SEQ2 block
        self.pandabox.send(Put("SEQ2.PRESCALE.UNITS", "s"))
        self.pandabox.send(Put("SEQ2.PRESCALE", f"{exposure_time}"))

        # enable blocks only
        self.enable_blocks(1)

    def PreStartOne(self, axis):
        """PreStart the specified trigger"""
        return True

    def StartOne(self, axis):
        """
        start PandABox master trigger
        """
        # scans on time domain
        self.panda.send(Put("BITS1.B", "0"))
        self.panda.send(Put("BITS1.B", "1"))

    def StateAll(self):
        """
        Get the trigger/gate state
        """
        try:
            self._state = self.motor_device.state()
            self._status = self.motor_device.status()
        except Exception as e:
            self._state = DevState.FAULT
            self._status = f"PandABox is in FAULT state: {e}"

    def StateOne(self, axis=1):
        """
        Get the trigger/gate state
        """
        return self._state, self._status

    def AbortOne(self, axis):
        """
        Disables SEQ block
        """
        self.enable_blocks(0)

    #### ---- Helpers ---- ####

    def enable_blocks(self, value):
        """
        enable/disable blocks according to the scan type
        """
        if value:
            self.panda.send(Put("BITS1.A", "0"))
            self.panda.send(Put("BITS1.A", "1"))
        else:
            self.panda.send(Put("BITS1.A", "0"))

    def _mm_to_encoder_counts(self, mm):
        """Convert mm to encoder counts at IcePAP
        Sardana motor positions are given in physical units:
        ```
        motor.position = [ motor.sign * motor.encodersourceformula(e_{ipap}) ] + motor.offset
        => motor.encodersourceformula(e_{ipap}) = (motor.position - motor.offset) / motor.sign
        ```
        Assuming `motor.encodersourceformula(e) = a1 * e` (i.e. linear
        relationship), can compute encoder counts at IcePAP;
        ```
        a1 = motor.encodersourceformula(e=1)
        => e_{ipap} = (motor.position - motor.offset) / (motor.sign * motor.encodersourceformula(1))
        ```

        """
        # Compute 1 encoder count in mm
        VALUE = 1  # Required for EncoderSourceFormula eval    # noqa
        enc_formula = eval(self.motor_device.EncoderSourceFormula)

        # Convert from mm to encoder counts @ IcePAP and the same as PandA
        return int(
            (mm - self.motor_device.offset) / (self.motor_device.sign * enc_formula)
        )

    @staticmethod
    def seq_table(repeats, trigger, positions, time1, phase1, time2, phase2):
        """
        Function to encode (repeats, trigger, phase1, phase2) into 32bit integer (code).

        Return a list [code, position, time1, time2] to send to pandabox sequencer (SEQ)
        table.
        """
        trigger_options = {
            "Immediate": 0,
            "bita=0": 1,
            "bita=1": 2,
            "bitb=0": 3,
            "bitb=1": 4,
            "bitc=0": 5,
            "bitc=1": 6,
            "posa>=position": 7,
            "posa<=position": 8,
            "posb>=position": 9,
            "posb<=position": 10,
            "posc>=position": 11,
            "posc<=position": 12,
        }

        # _b binary code
        repeats_b = "{0:016b}".format(repeats)  # 16 bits
        trigger_b = "{0:04b}".format(trigger_options[trigger])  # 4 bits (17-20)
        phase1_b = ""
        for key, value in sorted(phase1.items()):  # 6 bits (a-f)
            phase1_b = "1" + phase1_b if value else "0" + phase1_b
        phase2_b = ""
        for key, value in sorted(phase2.items()):  # 6 bits (a-f)
            phase2_b = "1" + phase2_b if value else "0" + phase2_b
        code_b = phase2_b + phase1_b + trigger_b + repeats_b  # 32 bits code
        code = int(code_b, 2)

        # One row of table = [code, position, time1, time2]
        pos_cmds = []
        for position in positions:
            pos_cmds.extend(
                [
                    f"{code:d}",
                    f"{int(position):d}",
                    f"{int(time1):d}",
                    f"{int(time2):d}",
                ]
            )

        return pos_cmds
